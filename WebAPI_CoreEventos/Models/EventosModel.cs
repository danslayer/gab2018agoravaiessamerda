﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_CoreEventos.Models
{
    public class EventosModel
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Descricao { get; set; }
        public string UrlImagem { get; set; }
        public string ValorAcao { get; set; }

        //public EventosModel GetEventosModel()
        //{
        //    //BUSCAR DO BANCO DE DADOS
        //    EventosModel evt = new EventosModel();
        //    evt.Titulo = "Global Azure Bootcamp";
        //    evt.Subtitulo = "Vale do Aço";
        //    evt.Descricao = "Saiba mais sobre o evento";
        //    evt.UrlImagem = "https://global.azurebootcamp.net/wp-content/uploads/2014/11/logo-2018-500x444-300x266.png";
        //    evt.ValorAcao = "https://global.azurebootcamp.net/";

        //    //return evt;


        //    EventosModel evt2 = new EventosModel();
        //    evt.Titulo = "Join Community";
        //    evt.Subtitulo = "Em Junho";
        //    evt.Descricao = "Garanta já o seu ingresso";
        //    evt.UrlImagem = "https://global.azurebootcamp.net/wp-content/uploads/2014/11/logo-2018-500x444-300x266.png";
        //    evt.ValorAcao = "https://global.azurebootcamp.net/";

        //    return evt2;
        //}

        public List<EventosModel> GetEventosModel()
        {
            List<EventosModel> eventos = new List<EventosModel>();

            //BUSCAR DO BANCO DE DADOS
            EventosModel evt = new EventosModel();
            evt.Titulo = "Global Azure Bootcamp";
            evt.Subtitulo = "Vale do Aço";
            evt.Descricao = "Saiba mais sobre o evento";
            evt.UrlImagem = "https://global.azurebootcamp.net/wp-content/uploads/2014/11/logo-2018-500x444-300x266.png";
            evt.ValorAcao = "https://global.azurebootcamp.net/";

            //return evt;

            EventosModel evt2 = new EventosModel();
            evt2.Titulo = "Join Community";
            evt2.Subtitulo = "Em Junho";
            evt2.Descricao = "Garanta já o seu ingresso";
            evt2.UrlImagem = "http://www.rccsp.org.br/wp-content/uploads/2015/04/novidades.jpg";
            evt2.ValorAcao = "https://global.azurebootcamp.net/";

            //return evt2;

            eventos.Add(evt);
            eventos.Add(evt2);

            return eventos;
        }
    }
}
