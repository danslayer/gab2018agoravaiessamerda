﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI_CoreEventos.Models;

namespace WebAPI_CoreEventos.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class EventosController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await ListarEventos();
        }

        private async Task<IActionResult> ListarEventos()
        {
            EventosModel eventos = new EventosModel();
            return Ok(eventos.GetEventosModel());
        }
    }
}