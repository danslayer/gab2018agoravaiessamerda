﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class AgendaDialog : IBaseDialogCard
    {
        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetThumbnailCard(
                    "Dia 06",
                    "",
                    "Programação dia 06/04",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/06.png "),
                    new CardAction(ActionTypes.DownloadFile, "Baixar", value: "https://github.com/walldba/JL-Project/raw/master/Menu_Mvp/PDF/AGENDA%20DIA%2006.pdf")),
                card.GetThumbnailCard(
                    "Dia 07",
                    "",
                    "Programação dia 07/04",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/07.png"),
                    new CardAction(ActionTypes.DownloadFile, "Baixar", value: "https://github.com/walldba/JL-Project/raw/master/Menu_Mvp/PDF/AGENDA%20DIA%2007.pdf")),
                 card.GetThumbnailCard(
                    "Voltar ao Menu",
                    "Não estava aqui o que procurava?",
                    "",
                    new CardImage(url: "http://sustainablearchaeology.org/img/backtomenu.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu")),                
            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Call(new OEventoDialog(), null);
            }
            else if (message.Text.ToLower().Contains("Dia 06"))
            {
                context.Call(new AgendaDialog(), null);
            }
            else if (message.Text.ToLower().Contains("Dia 07"))
            {
                context.Call(new AgendaDialog(), null);
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Confira aqui a programação completa do evento:");
            Thread.Sleep(3000);

            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();
            //reply.Value = matriculaTratada;

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);

        }
    }
}