﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using poc_architecture_netfram_v2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class EventosDialog:IBaseDialogCard
    {
        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();
            return new List<Attachment>()
            {
                card.GetThumbnailCard(
                    "Edgard Barros",
                    "CEO Portnet | Como vender a Nuvem?",
                    "09:30",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/TrilhaAzure.png "),
                    new CardAction(ActionTypes.OpenUrl,"www.google.com", value: "")),
               
            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Call(new PrincipalDialog(new MVPConfDialog(), new OEventoDialog(), new Patrocinadores(), new RootLUISDialog(), new EventosDialog()), null);
            }

        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Falar sobre gab");
            Thread.Sleep(3000);
            context.Call(new PrincipalDialog(new MVPConfDialog(), new OEventoDialog(), new Patrocinadores(), new RootLUISDialog(), new EventosDialog()), null);
            //var reply = context.MakeMessage();
            //reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            //reply.Attachments = GetCardsAttachments();

            //await context.PostAsync(reply);
            //context.Wait(this.MessageReceivedAsyncCardAction);
        }
    }
}