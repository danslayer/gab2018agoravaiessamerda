﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class TrilhasEventoDialog : IBaseDialogCard
    {
        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetThumbnailCard(
                    "Edgard Barros",
                    "CEO Portnet | Como vender a Nuvem?",
                    "09:30",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/TrilhaAzure.png "),
                    new CardAction(ActionTypes.OpenUrl,"www.google.com", value: "")),
                card.GetThumbnailCard(
                    "Thiago Nascimento",
                    "Desenvolvendo ChatBots inteligentes",
                    "10:00",
                    new CardImage(url: "https://media.licdn.com/dms/image/C4D03AQGbpFL3OjIlPg/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=vXirgEHFujsijbeqizN5ThTB9Qrq2L8xlSxrWpoWgxM "),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "")),
                card.GetThumbnailCard(
                    "Andre Paulovich",
                    "Desenvolvendo e publicando Apps para Azure AKS com .NET core",
                    "10:30",
                    new CardImage(url: "https://media.licdn.com/dms/image/C4D03AQEjvsQlv9ynug/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=S4SF-z22dVYRBoKt4gvKvzaJbjHyKj3l7dILaoZCUAY"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "Conteineres")),
                card.GetThumbnailCard(
                    "Luciana e Arthur",
                    "Machine Learning: Hands On no Azure Machine Learning Studio",
                    "13:30",
                    new CardImage(url: "https://i.imgur.com/H5fPjZr.png "),
                    new CardAction(ActionTypes.DownloadFile, "Baixar", value: "https://github.com/walldba/JL-Project/raw/master/Menu_Mvp/PDF/trilhaDevops.pdf")),
                card.GetThumbnailCard(
                    "Gustavo Magella [MVP]",
                    "Microsoft Azure, o poder da Nuvem!",
                    "Saiba tudo sobre:",
                    new CardImage(url: "https://media.licdn.com/dms/image/C5103AQEI-ZFsmphvnQ/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=qBbt-poJ-aE0LgexCfH-sG8lQwTlk7GXf9245yPTlA8 "),
                    new CardAction(ActionTypes.DownloadFile, "Baixar", value: "https://github.com/walldba/JL-Project/raw/master/Menu_Mvp/PDF/trilhaDevops.pdf")),
                card.GetThumbnailCard(
                    "A definir",
                    "A definir",
                    "14:30",
                    new CardImage(url: "https://media.licdn.com/dms/image/C5103AQEI-ZFsmphvnQ/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=qBbt-poJ-aE0LgexCfH-sG8lQwTlk7GXf9245yPTlA8"),
                    new CardAction(ActionTypes.DownloadFile, "Baixar", value: "https://github.com/walldba/JL-Project/raw/master/Menu_Mvp/PDF/trilhaDevops.pdf")),
            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Call(new PrincipalDialog(new MVPConfDialog(), new OEventoDialog(), new Patrocinadores(), new RootLUISDialog(), new EventosDialog()), null);
            }
            else if (message.Text.ToLower().Contains("azure"))
            {
                await context.PostAsync("Trilha azure");
            }
            else if (message.Text.ToLower().Contains("Conteineres"))
            {
                await context.PostAsync("Trilha Conteineres");
            }
            else if (message.Text.ToLower().Contains("DevOps"))
            {
                await context.PostAsync("Trilha devops");
            }
            else if (message.Text.ToLower().Contains("Empreendedorismo"))
            {
                await context.PostAsync("Trilha Empreendedorismo");
            }
            else if (message.Text.ToLower().Contains("IA e BigData"))
            {
                await context.PostAsync("IA e BigData");
            }
            else if (message.Text.ToLower().Contains("IoT"))
            {
                await context.PostAsync("Trilha IoT");
            }
            else if (message.Text.ToLower().Contains("ITPro e Segurança"))
            {
                await context.PostAsync("Trilha ITPro e Segurança");
            }
            else if (message.Text.ToLower().Contains("Mobilidade"))
            {
                await context.PostAsync("Trilha Mobilidade");
            }
            else if (message.Text.ToLower().Contains("Office 365"))
            {
                await context.PostAsync("Trilha Office 365");
            }
            else if (message.Text.ToLower().Contains("Visual Studio"))
            {
                await context.PostAsync("Trilha Visual Studio");
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Navegue pelas trilhas e veja as palestras:");
            Thread.Sleep(3000);
            
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);
        }



        public IList<Attachment> GetAzureCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetHeroCard(
                    "Azure SQL Database - Scaling In e Scaling Out através do elastic pool",
                    "Vitor Fava",
                    "09:00 - 09:55",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/agendamvp.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/microsoft-azure-aprenda-do-zero/")),
                card.GetHeroCard(
                    "Contêineres",
                    "4 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/comoChegar.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/microsoft-azure-backup-proteja-seu-ambiente/")),
                card.GetHeroCard(
                    "DevOps",
                    "6 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/palestrantes.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "Empreendedorismo",
                    "9 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "IA & BigData",
                    "10 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "IoT",
                    "4 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "ITPro & Segurança",
                    "13 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "Mobilidade",
                    "3 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "Office 365",
                    "10 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                 card.GetHeroCard(
                    "Visual Studio",
                    "9 palestras",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/trilhas.png"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "https://www.udemy.com/seguranca-da-informacao-guia-basico-nbr-isoiec-27002/")),
                card.GetHeroCard(
                    "Voltar ao Menu",
                    "Não estava aqui o que procurava?",
                    "",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/Voltar.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu")),
            };
        }
    }
}