﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class OEventoDialog: IBaseDialogCard
    {
        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();
            return new List<Attachment>()
            {
                card.GetThumbnailCard(
                    "Edgard Barros",
                    "CEO Portnet | Como vender a Nuvem?",
                    "09:30",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/TrilhaAzure.png "),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "Edgard Barros")),
                card.GetThumbnailCard(
                    "Thiago Nascimento",
                    "Desenvolvendo ChatBots inteligentes",
                    "10:00",
                    new CardImage(url: "https://media.licdn.com/dms/image/C4D03AQGbpFL3OjIlPg/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=vXirgEHFujsijbeqizN5ThTB9Qrq2L8xlSxrWpoWgxM "),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "")),
                card.GetThumbnailCard(
                    "Andre Paulovich",
                    "Desenvolvendo e publicando Apps para Azure AKS com .NET core",
                    "10:30",
                    new CardImage(url: "https://media.licdn.com/dms/image/C4D03AQEjvsQlv9ynug/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=S4SF-z22dVYRBoKt4gvKvzaJbjHyKj3l7dILaoZCUAY"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "Conteineres")),
                card.GetThumbnailCard(
                    "Luciana e Arthur",
                    "Machine Learning: Hands On no Azure Machine Learning Studio",
                    "13:30",
                    new CardImage(url: "https://i.imgur.com/H5fPjZr.png "),
                   new CardAction(ActionTypes.ImBack, "Confira", value: "Conteineres")),
                card.GetThumbnailCard(
                    "Gustavo Magella [MVP]",
                    "Microsoft Azure, o poder da Nuvem!",
                    "Saiba tudo sobre:",
                    new CardImage(url: "https://media.licdn.com/dms/image/C5103AQEI-ZFsmphvnQ/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=qBbt-poJ-aE0LgexCfH-sG8lQwTlk7GXf9245yPTlA8 "),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "Conteineres")),
                card.GetThumbnailCard(
                    "A definir",
                    "A definir",
                    "14:30",
                    new CardImage(url: "https://media.licdn.com/dms/image/C5103AQEI-ZFsmphvnQ/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=qBbt-poJ-aE0LgexCfH-sG8lQwTlk7GXf9245yPTlA8"),
                    new CardAction(ActionTypes.ImBack, "Confira", value: "Conteineres")),
                card.GetThumbnailCard(
                    "Voltar ao menu",
                    "",
                    "Menu",
                    new CardImage(url: "https://media.licdn.com/dms/image/C5103AQEI-ZFsmphvnQ/profile-displayphoto-shrink_800_800/0?e=1529193600&v=beta&t=qBbt-poJ-aE0LgexCfH-sG8lQwTlk7GXf9245yPTlA8"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "menu")),
            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Call(new PrincipalDialog(new MVPConfDialog(), new OEventoDialog(), new Patrocinadores(), new RootLUISDialog(), new EventosDialog()), null);
            }
            else if (message.Text.ToLower().Contains("Edgard Barros"))
            {
                await context.PostAsync("Não sei");
                context.Call(new OEventoDialog(),null);
            }
            else if (message.Text.ToLower().Contains("Thiago Nascimento"))
            {
                await context.PostAsync("Thiago é pos graduado em arquitetura de softwares, trabalha como arquiteto de sistemas há mais de 6 anos, e hoje lidera uma celula de chatbots e inovação em uma das maiores empresas de Call Center da America Latina.");
                context.Call(new OEventoDialog(), null);
            }
            else if (message.Text.ToLower().Contains("Andre Paulovich"))
            {
                await context.PostAsync("Arquiteto de software e desenvolvedor há mais de 10 anos de experiencia no mercado, hoje é lider tecnico em uma das empresas mais diferenciadas e ageis do mercado de software brasileiro");
                context.Call(new OEventoDialog(), null);
            }
            else if (message.Text.ToLower().Contains("Luciana e Arthur"))
            {
                await context.PostAsync("Luciana é formada em Estatistica e Pós Graduada em B.I, trabalha há 10 anos na área e hojé é peça chave em uma das empersas mais promissoras no ramo de ciência de dados da atualidade.");
                await context.PostAsync("Arthur é bacharel em Sistemas de Informação e Mestre em Ciência da Computação. Trabalha hoje em uma celula de inovação e ciência de dados, em uma das maiores empresas de Call Center da América Latina.");
                context.Call(new OEventoDialog(), null);
            }
            else if (message.Text.ToLower().Contains("Gustavo Magella [MVP]"))
            {
                await context.PostAsync("Gustavo é MVP na categoria Azure, atua como arquiteto de CLloud e Infraestrutura em uma das empresas pioneiras de soluções tecnologicas para o ramo de seguros e cobrança.");
                context.Call(new OEventoDialog(), null);
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Vejá aqui os horarios dos palestrantes");
            Thread.Sleep(3000);

            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);
        }
       
    }
}