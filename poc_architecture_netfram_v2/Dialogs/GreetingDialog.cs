﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class GreetingDialog : IDialog<IMessageActivity>
    {
        const string key_client_user_name = "Name";
        private readonly PrincipalDialog principalDialog;
        //private readonly HardwareCardsDialog hardwareDialog;
        private readonly MVPConfDialog sobreAutorDialog;

        public string turno = "";
        public int hora = DateTime.Now.Hour;

        public GreetingDialog()
        {
            
        }

        public GreetingDialog(PrincipalDialog principalDialog, MVPConfDialog sobreAutorDialog)
        {
            this.principalDialog = principalDialog;            
            this.sobreAutorDialog = sobreAutorDialog;
        }

        //public GreetingDialog(PrincipalDialog principalDialog, HardwareCardsDialog hardwareDialog, SobreAutorDialog sobreAutorDialog)
        //{
        //    this.principalDialog = principalDialog;
        //    this.hardwareDialog = hardwareDialog;
        //    //this.sobreAutorDialog = sobreAutorDialog;
        //}

        public async Task StartAsync(IDialogContext context)
        {

            if (turno == "")
            {
                if (hora > 17)
                {
                    turno = "Boa Noite";
                }
                else if (hora > 11)
                {
                    turno = "Boa Tarde";
                }
                else
                {
                    turno = "Bom dia";
                }
            }
            await context.PostAsync($"{turno}, eu sou a Gab, assistente de um dos maiores eventos de todo o mundo, a **Global Azure Bootcamp**!");
            Thread.Sleep(3000);
            await context.PostAsync($"Veja abaixo em que eu posso lhe ajudar:");
            Thread.Sleep(3000);
            context.Call(this.principalDialog, this.principalDialog.MessageResumeAfter);

            //var reply = context.MakeMessage();
            //Thread.Sleep(1000);

            //reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            //reply.Attachments = GetCardsAttachments();
            //await context.PostAsync(reply);

        }

        

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> messageActivity)
        {
            
        }

        private static void SetUserName(IDialogContext context, string value)
        {
            context.UserData.SetValue<string>(key_client_user_name, value);
        }

        private static string GetUserName(IDialogContext context)
        {
            var userName = String.Empty;
            context.UserData.TryGetValue<string>(key_client_user_name, out userName);
            return userName;
        }
    }
}