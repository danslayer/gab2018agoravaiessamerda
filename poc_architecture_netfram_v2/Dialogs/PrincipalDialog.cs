﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class PrincipalDialog : IBaseDialogCard
    {
        const string key_client_user_name = "Name";
        //private readonly HardwareCardsDialog hardwareDialog;
        private readonly MVPConfDialog sobreAutorDialog;
        private readonly OEventoDialog cursosDialog;
        private readonly Patrocinadores certificacoesCurriculoDialog;
        private readonly RootLUISDialog rootLuisDialog;
        private readonly EventosDialog eventosDialog;

        public PrincipalDialog(MVPConfDialog sobreAutorDialog, OEventoDialog cursosDialog, Patrocinadores certificacoesCurriculoDialog, RootLUISDialog rootLuisDialog, EventosDialog eventosDialog)
        {
            this.sobreAutorDialog = new MVPConfDialog();
            this.cursosDialog = new OEventoDialog();
            this.certificacoesCurriculoDialog = new Patrocinadores();
            this.eventosDialog = new EventosDialog();
            this.rootLuisDialog = new RootLUISDialog();
        }

        /// <summary>
        /// Método inicial, chamado assim que o objeto é criado
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task StartAsync(IDialogContext context)
        {
            //Thread.Sleep(1000);

            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {

            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("palestras"))
            {
                context.Call(this.cursosDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("o que é gab"))
            {                
                context.Call(this.eventosDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("evento"))
            {
                context.Call(this.sobreAutorDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("patrocinadores"))
            {
                context.Call(this.certificacoesCurriculoDialog, this.MessageResumeAfter);
            }

            else if (message.Text == "Voltar ao menu")
            {
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = GetCardsAttachments();
                await context.PostAsync(reply);
            }
            else if (message.Text == "sorteio")
            {
                await context.PostAsync("Envie para este email > **dans.dnf@gmail.com** com o assunto, GAB, por qual motivo merece ganhar o ingresso para o Global azure bootcamp");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = GetCardsAttachments();
                await context.PostAsync(reply);
            }
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {

            throw new NotImplementedException();
        }

        private Task AfterDialogComplete(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        private static IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetHeroCard(
                    "O que é o Global Azure Bootcamp.",
                    "Teste",
                    "Saiba aqui o que é o MVP conf",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/Oque.png"),
                    new CardAction(ActionTypes.ImBack, "Saiba mais", value: "o que é gab")),
                card.GetHeroCard(
                    "O Evento",
                    "",
                    "Saiba tudo do evento aqui",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/Oque.png"),
                    new CardAction(ActionTypes.ImBack, "Saiba mais", value: "Evento")),
                card.GetHeroCard(
                    "Apoiadores e Patrocinadores",
                    "",
                    "Conheça todos os nossos apoiadores e patrocinadores",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/Oque.png"),
                    new CardAction(ActionTypes.ImBack, "Saiba mais", value: "Apoiadores e Patrocinadores")),
                card.GetHeroCard(
                    "Palestras",
                    "",
                    "Saiba quais seram as palestras",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/Oque.png"),
                    new CardAction(ActionTypes.ImBack, "Saiba mais", value: "Palestras")),
                card.GetHeroCard(
                    "Ganhe seu ingresso",
                    "",
                    "Para tentar ganhar seu convite para o GAB",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Mvp/Oque.png"),
                    new CardAction(ActionTypes.ImBack, "Saiba mais", value: "sorteio")),
            };
        }
        
        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            await context.PostAsync("COLÉ");
        }

        IList<Attachment> IBaseDialogCard.GetCardsAttachments()
        {
            throw new NotImplementedException();
        }
    }
}