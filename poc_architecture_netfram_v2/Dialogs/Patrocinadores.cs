﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class Patrocinadores : IBaseDialogCard
    {
        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();
            
            return new List<Attachment>()
            {                 
                card.GetThumbnailCard2(
                    "UAI Solutions",
                    "",
                    "",
                    new CardImage(url: "http://uaisolutions.com.br/images/icon.png")),
                card.GetThumbnailCard(
                    "Voltar ao Menu",
                    "Não estava aqui o que procurava? Volte ao Menu :D",
                    "",
                    new CardImage(url: "http://sustainablearchaeology.org/img/backtomenu.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu")),
            };
        }

        private static List<CardAction> CurriculoCardsAction()
        {
            List<CardAction> listaCardAction = new List<CardAction>();
            listaCardAction.Add(new CardAction(ActionTypes.DownloadFile, "PT", value: "http://gustavomagella.com/wp-content/uploads/2017/03/1_CV-GustavoMagella-2017-1.pdf"));
            listaCardAction.Add(new CardAction(ActionTypes.DownloadFile, "EN", value: "http://gustavomagella.com/wp-content/uploads/2017/03/1_CV-GustavoMagella-2017-1.pdf"));
            //listaCardAction.Add(new CardAction(ActionTypes.ImBack, "PT", value: "PT-BR"));
            //listaCardAction.Add(new CardAction(ActionTypes.ImBack, "EN", value: "EN-US"));
            return listaCardAction;
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Call(new PrincipalDialog(new MVPConfDialog(), new OEventoDialog(), this, new RootLUISDialog(), new EventosDialog()), null);
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(IDialogContext context)
        {          
            await context.PostAsync("Confira aqui quem apoia o MVPConf 2018:");
            Thread.Sleep(3000);

            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);
        }
    }
}