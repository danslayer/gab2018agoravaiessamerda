﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [LuisModel("f9096b26-6cd2-4190-822b-30d845ab1d6f", "8886713d17884b4a9a8bff1a5c48f0dc")]
    [Serializable]

    public class RootLUISDialog : LuisDialog<bool>
    {
        //Opções
        public const string OptionSim = "Sim";
        public const string OptionNao = "Voltar ao Menu";
        public const string PodeSimOption = "Pode sim =D";
        public const string NaoQueroOption = "Não. Quero sair do diálogo.";

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Então, conversa franca... Fala aí que se eu for esperto o suficiente eu te respondo!");
            Thread.Sleep(3000);
            await context.PostAsync("Cuidado com as pegadinhas hein!");
            Thread.Sleep(3000);

            //var reply = context.MakeMessage();
            //reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            //reply.Attachments = GetCardsAttachments();

            //await context.PostAsync(reply);
            //context.Wait(this.MessageReceivedAsyncCardAction);
        }

        [LuisIntent("None")]
        public async Task NoneAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Desculpe! Não entendi. Você pode tentar escrever novamente de outra forma, ou voltar ao Menu e navegar pelas opções.");
            await PromptChoiceAsync(context, result, "**Deseja digitar novamente?**");
            
            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        [LuisIntent("CuriosidadesBOT")]
        public async Task CuriosidadesBOTAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Boa pergunta. Então, um BOT é um programa de computador que tenta simular um humano na conversação com as pessoas. ");
            Thread.Sleep(2000);
            await context.PostAsync("Eu sou um Chatbot. Posso entender algumas perguntas e responder naturalmente, já outras coisas eu não entendo muito bem ou ainda não fui treinado para responder.");
            Thread.Sleep(2000);
            await context.PostAsync("Tenho alguma inteligência e meus desenvolvedores sempre mexem comigo, me deixando mais inteligente a cada dia! ");

            await PromptChoiceAsync(context, result, "Posso lhe ajudar com algo mais?");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        [LuisIntent("InformarIdiomas")]
        public async Task InformarIdiomasAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Tenho um bom conhecimento de Inglês, já tive a oportunidade de fazer algumas viagens aos USA e Canadá, que foram muito proveitosas nesse sentido. ");
            await PromptChoiceAsync(context, result, "Posso lhe ajudar com algo mais?");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        [LuisIntent("InformarIdade")]
        public async Task InformarIdadeAsync(IDialogContext context, LuisResult result)
        {
            int anos = CalcularIdade();

            await context.PostAsync($"Tenho {anos} anos.");
            await PromptChoiceAsync(context, result, "Posso lhe ajudar com algo mais?");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }
        
        [LuisIntent("ElogiarBot")]
        public async Task ElogiarBotAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync($"Obrigado pelo Elogio! Fico feliz em poder ajudar!");
            await PromptChoiceAsync(context, result, "Posso lhe ajudar com algo mais?");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        [LuisIntent("InformarFormacaoAcademica")]
        public async Task InformarFormacaoAcademicaAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync($"Sou Formado em ...");
            await PromptChoiceAsync(context, result, "Posso lhe ajudar com algo mais?");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }


        private int CalcularIdade()
        {            
            return Convert.ToInt32(DateTime.Now.Year - 1989);
        }

        public async Task PromptChoiceAsync(IDialogContext context, LuisResult result, string texto)
        {

            PromptDialog.Choice(
            context,
            this.AfterChoiceSelected,
            new[] { OptionSim, OptionNao, NaoQueroOption },
            texto,
            "Me desculpe, mas eu preciso que você responda se eu posso ou não te ajudar com algum outro problema.:",
            attempts: 2);
        }

        public async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;
                switch (selection)
                {

                    //case OptionSim:
                    //    await context.PostAsync("Ah, que bom que pude lhe ajudar! \n\n **#EstamosJuntos** \n\n **#NossoCoraçãoÉDigital**💙");
                    //    PromptDialog.Choice(
                    //     context,
                    //     this.AfterChoiceSelected,
                    //     new[] { PodeSimOption, NaoQueroOption },
                    //     "**Posso te ajudar com algum outro problema?**",
                    //     "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                    //     attempts: 2);
                    //    break;

                    case OptionSim:
                        await context.PostAsync("Ah, que legal! Então vamos lá:");
                        //PromptDialog.Choice(
                        // context,
                        // this.AfterChoiceSelected,
                        // new[] { PodeSimOption, NaoQueroOption },
                        // "**Posso te ajudar com algum outro problema?**",
                        // "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                        // attempts: 2);
                        break;
                    case OptionNao:
                        await context.PostAsync("Ah, OK! Voltando ao Menu em 3, 2, 1...");
                        Thread.Sleep(3000);
                        context.Call(new PrincipalDialog(null, null, null, this, null), null);
                        //PromptDialog.Choice(
                        //context,
                        //this.AfterChoiceSelected,
                        //new[] { PodeSimOption, NaoQueroOption },
                        //"**Posso te ajudar com algum outro problema?** ",
                        //"Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                        //attempts: 2);
                        break;

                    case PodeSimOption:
                        context.Done(false);
                        break;

                    case NaoQueroOption:
                        await context.PostAsync("Valeu demais por você aparecer por aqui! Não esquece de curtir nossa página no Facebook hein? (gustavomagellati) Um bjo no coração <3 e #borapranuvem ! Gustavo Magella | MVP Microsoft Azure");
                        context.Call(new GreetingDialog(), null);
                        
                        //await this.StartAsync(context);
                        break;                   
                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }

        }

        public async Task AfterLuisDialog(IDialogContext context, IAwaitable<bool> result)
        {
            var success = await result;

            if (!success)
            {
                await context.PostAsync("Desculpe, não entendi.");
            }

            await this.StartAsync(context);
        }
    }
}