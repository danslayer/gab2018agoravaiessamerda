﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class MVPConfDialog : IBaseDialogCard
    {                
        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {                 
                card.GetThumbnailCard(
                    "Se inscreva",
                    "Acesse aqui",
                    "",
                    new CardImage(url: "https://global.azurebootcamp.net/wp-content/uploads/2014/11/logo-2018-500x444-300x266.png"),
                    new CardAction(ActionTypes.OpenUrl, "Acesse", value: "https://www.sympla.com.br/global-azure-bootcamp-2018---bhmg---raja-valley__268314")),
                card.GetThumbnailCard(
                    "Site",
                    "Acesse aqui",
                    "",
                    new CardImage(url: "https://global.azurebootcamp.net/wp-content/uploads/2014/11/logo-2018-500x444-300x266.png"),
                    new CardAction(ActionTypes.OpenUrl, "Acesse", value: "https://gabbh.azurewebsites.net/#latestnews")),
                card.GetThumbnailCard(
                    "Facebook",
                    "Seja meu amigo no Face",
                    "",
                    new CardImage(url: "https://blog.transparentcareer.com/wp-content/uploads/2017/01/facebook2.png"),
                    new CardAction(ActionTypes.OpenUrl, "Acesse", value: "https://www.facebook.com/globalazurebh/")),
                  card.GetThumbnailCard(
                    "Como chegar",
                    "Acesse aqui",
                    "",
                    new CardImage(url: "https://global.azurebootcamp.net/wp-content/uploads/2014/11/logo-2018-500x444-300x266.png"),
                    new CardAction(ActionTypes.OpenUrl, "Acesse", value: "https://www.google.com.br/maps/dir//Raja+Valley+-+Av.+Raja+Gab%C3%A1glia,+4343+-+Cidade+Jardim,+Belo+Horizonte+-+MG,+30350-577/@-19.9680307,-43.9563673,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0xa697ee15f31f5d:0x779852a1741039bb!2m2!1d-43.9541786!2d-19.9680307!3e0")),
                card.GetThumbnailCard(
                    "Voltar ao Menu",
                    "Não estava aqui o que procurava?",
                    "",
                    new CardImage(url: "http://sustainablearchaeology.org/img/backtomenu.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu")),
            };
        }
        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {

            //throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Call(new PrincipalDialog(this, new OEventoDialog(), new Patrocinadores(), new RootLUISDialog(), new EventosDialog()), null);                
            }
        }

        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            await this.MessageReceivedAsyncCardAction(context, null);               
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Junte-se a usuários do Microsoft Azure do mundo todo em 21 de abril de 2018 no sexto evento anual Global Azure Bootcamp.");
            Thread.Sleep(3000);
            await context.PostAsync("Encontre outros membros da comunidade do Azure para compartilhar habilidades essenciais e ver as coisas incríveis que as pessoas estão fazendo com os serviços do Azure.");
            //context.Call(new PrincipalDialog(this, new OEventoDialog(), new Patrocinadores(), new RootLUISDialog(), new EventosDialog()), null);


            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);
        }
    }
}