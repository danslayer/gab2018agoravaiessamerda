﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_architecture_netfram_v2.Dialogs.Interfaces
{
    public interface IBaseDialogForm<TModel>
    {
        IDialog<TModel> Build();
        Task ResumeAfter(IDialogContext context, IAwaitable<TModel> model);
    }
}
