﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Infrastructure.Logging
{
    public class QueueLoggerSettings
    {
        public QueueLoggerSettings()
        {
            CompressMessage = false;
            OverflowHanding = LargeMessageMode.Discard;
            MessageTrimRate = 0.10f;
        }

        public bool CompressMessage { get; set; }
        public LargeMessageMode OverflowHanding { get; set; }
        public float MessageTrimRate;
    }
}