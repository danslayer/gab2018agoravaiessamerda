﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Services
{
    [Serializable]
    public class EventosAPIServices
    {
        public RestClient _client;

        public EventosAPIServices()
        {
            _client = new RestClient();
            //_client.BaseUrl = new System.Uri("http://localhost:54997/");
            _client.BaseUrl = new System.Uri("http://ghmmwebapieventos.azurewebsites.net/");
        }

        public List<Eventos> ListarEventos()
        {
            List<Eventos> result = new List<Eventos>();
            var request = new RestRequest($"api/Eventos", Method.GET);

            var response = _client.Execute(request);

            try
            {
                //IEnumerable<Eventos> chamados = JsonConvert.DeserializeObject<IEnumerable<Eventos>>(response.Content);
                IEnumerable<Eventos> chamados = JsonConvert.DeserializeObject<IEnumerable<Eventos>>(response.Content);
                //Eventos chamados = JsonConvert.DeserializeObject<Eventos>(response.Content);

                foreach (var item in chamados)
                {
                    result.Add(item);
                }                
            }
            catch (Exception ex)
            {
                throw new Exception("Mensagem: " + ex.Message + ex.InnerException.Message);
            }

            return result;
        }
    }
}