﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Services
{
    public class Eventos
    {        
        public string titulo { get; set; }
        public string subtitulo { get; set; }
        public string descricao { get; set; }
        public string urlImagem { get; set; }
        public string valorAcao { get; set; }
    }
}