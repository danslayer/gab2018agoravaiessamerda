﻿using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Models.Cards
{
    public class CardModel
    {
        public CardModel ()
        {

        }

        public Attachment GetHeroCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)
        {
            var heroCard = new HeroCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,
                Images = new List<CardImage>() { cardImage },
                Buttons = new List<CardAction>() { cardAction },
            };

            return heroCard.ToAttachment();
        }

        public Attachment GetThumbnailCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)
        {
            var heroCard = new ThumbnailCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,
                Images = new List<CardImage>() { cardImage },
                Buttons = new List<CardAction>() { cardAction },
            };

            return heroCard.ToAttachment();
        }

        public Attachment GetThumbnailCard2(string title, string subtitle, string text, CardImage cardImage)
        {
            var heroCard = new ThumbnailCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,
                Images = new List<CardImage>() { cardImage }    
                //Buttons = new List<CardAction>()                
                //Buttons = new List<CardAction>() { cardAction },
            };

            //foreach (var item in cardAction)
            //{
            //    heroCard.Buttons.Add(item);
            //}

            return heroCard.ToAttachment();
        }
    }
}