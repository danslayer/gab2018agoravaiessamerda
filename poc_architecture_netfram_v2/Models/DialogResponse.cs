﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Models
{
    public class DialogResponse
    {
        public string Message { get; set; }
        public object Data { get; set; }
    }
}