﻿using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using Microsoft.WindowsAzure.Storage;
using poc_architecture_netfram_v2.Dialogs;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Infrastructure.Modules;
using poc_architecture_netfram_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace poc_architecture_netfram_v2
{
    public class IocConfig
    {
        public IContainer ApplicationContainer { get; private set; }

        //public IServiceProvider ConfigureServices(IServiceCollection services)
        public static void Configure(HttpConfiguration config)        
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            containerBuilder.RegisterWebApiFilterProvider(config);

            containerBuilder.RegisterType<GreetingDialog>()
                .InstancePerDependency();
            containerBuilder.RegisterType<RootDialog>()
                .InstancePerDependency();
            containerBuilder.RegisterType<PrincipalDialog>()
                .InstancePerDependency();
            //containerBuilder.RegisterType<HardwareCardsDialog>()
            //    .InstancePerDependency();
            containerBuilder.RegisterType<MVPConfDialog>()
                .InstancePerDependency();
            containerBuilder.RegisterType<OEventoDialog>()
                .InstancePerDependency();
            containerBuilder.RegisterType<Patrocinadores>()
                .InstancePerDependency();
            containerBuilder.RegisterType<RootLUISDialog>()
                .InstancePerDependency();
            containerBuilder.RegisterType<EventosDialog>()
                .InstancePerDependency();

            //containerBuilder.RegisterType<OpcoesMouseDialog>()
            //    .As<IBaseDialogForm<OpcoesDialogService>>()
            //    .InstancePerDependency();


            //PARTE DO BANCO
            //Conversation.UpdateContainer(builder =>
            //{
            //    builder.RegisterModule(new AzureModule(Assembly.GetExecutingAssembly()));
            //    string uri = "https://oiapoccosmodb.documents.azure.com:443/";
            //    var uriServer = new Uri(uri);

            //    var storeDocument = new DocumentDbBotDataStore(uriServer,
            //        authKey: "2M3YPsSKe6mwPYUr1PH9T3WIeHle2Fg9k2O7Jyd4M3CUgjQFRpQAORXjWVSZnjBoUjgKAKAKJ0YHmGvd6pOY9Q==",
            //        databaseId: "oiapoccosmodb",
            //        collectionId: "_logs");

            //    builder.Register(c => storeDocument)
            //        .Keyed<IBotDataStore<BotData>>(AzureModule.Key_DataStore)
            //        .AsSelf()
            //        .SingleInstance();


            //    var storageAccount = CloudStorageAccount.Parse(connectionString: "DefaultEndpointsProtocol=https;AccountName=oiapocqueue;AccountKey=1Pd2snHIvwRbB98TIVtqCXzA1XrLLFvpPm2CyYpO+BpLhB0z3EwR/r3Iwf8/wnhd9T9cdsEu+0nSiaXwuw24FQ==;EndpointSuffix=core.windows.net");

            //    builder.RegisterModule(new QueueActivityModule(storageAccount, queueName: "activity-logger"));
            //});

            //FIM PARTE DO BANCO

            //builder.Update(Conversation.Container);

            //ApplicationContainer = new Conversation.Container;
            //return new AutofacServiceProvider(ApplicationContainer);

            var container = containerBuilder.Build();
            //builder.Update(Conversation.Container);

            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}