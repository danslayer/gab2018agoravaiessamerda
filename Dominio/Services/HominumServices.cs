﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Services
{
    [Serializable]
    public class HomunimServices
    {
        public RestClient _client;

        public HomunimServices()
        {
            _client = new RestClient();

            //WebProxy proxyLocal = new WebProxy("192.168.239.14", 8080);
            //proxyLocal.UseDefaultCredentials = false;
            //proxyLocal.Credentials = p;
            //_client.Proxy = proxyLocal;

            //_client.BaseUrl = new System.Uri("https://contactcenter1.aec.com.br/WebServices/AeC.Web.Services.Hominum/");
            _client.BaseUrl = new System.Uri("https://contactcenter1-ws.aec.com.br/WebServices/AeC.Web.Services.Hominum/");

            //_client = new RestClient(credential.BaseUrl)
            //    Authenticator = new HttpBasicAuthenticator("clientaec01", "p@ssw0ord"),
            //    Proxy = proxyLocal
            //};
        }
        public async Task<string> GetFuncionarioAsync(string matricula)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("token-ws", "921406d1-88aa-46cd-b793-1f36e5d6fda9");

                var defaultUrl = $"https://contactcenter1.aec.com.br/WebServices/AeC.Web.Services.Hominum/api/Hierarquia?$filter=Funcionario/Matricula eq '{matricula}' and Substituida eq false and Status eq '1'&$select=Funcionario/Nome";

                var response = await client.GetAsync(defaultUrl);
                var result = await response.Content.ReadAsStringAsync();

                IEnumerable<Class1> f = JsonConvert.DeserializeObject<IEnumerable<Class1>>(result);
                Class1 obj = f.FirstOrDefault();

                if (obj != null)
                {
                    return result = obj.Funcionario.Nome;
                }
                return response.ToString();
            }
            catch (Exception ex)
            {
                return $"{ex.GetBaseException().StackTrace} --message {ex.Message}";
            }
        }

        public string GetFuncionario(string matricula)
        {
            var result = new Funcionario();
            var request = new RestRequest("api/Hierarquia?$filter=Funcionario/Matricula eq '" + matricula + "' and Substituida eq false and Status eq '1'&$select=Funcionario/Nome", Method.GET);
            //request.AddParameter("type", "json");
            request.AddHeader("token-ws", "921406d1-88aa-46cd-b793-1f36e5d6fda9");

            //X509Certificate2 certificate2 = new X509Certificate2();
            //certificate2.Import();



            ServicePointManager.ServerCertificateValidationCallback = delegate (
            Object obj, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors errors)
            {
                return (true);
            };

            var response = _client.Execute(request);

            if (response.ErrorMessage != null)
            {
                string Cookies = "";
                foreach (var item in response.Cookies)
                {
                    Cookies += item.Name + " -- " + item.Value + " \n";
                }

                return "Mensagem de Erro: " + response.ErrorMessage + "  -- Exceção: " + response.ErrorException + "  -- Sucesso? " + response.IsSuccessful + " -- StatusCode " + response.StatusCode + " -- " + Cookies;
            }
            else
            {
                try
                {
                    IEnumerable<Class1> f = JsonConvert.DeserializeObject<IEnumerable<Class1>>(response.Content);
                    Class1 obj = f.First();
                    result = obj.Funcionario;
                }
                catch (Exception ex)
                {
                    return "Matriculanaoexiste";
                }


                try
                {
                    if (response.ErrorMessage != null)
                        throw new Exception(response.ErrorMessage);

                    if (response.StatusCode == HttpStatusCode.InternalServerError ||
                        response.StatusCode == HttpStatusCode.BadRequest)
                        throw new Exception(response.Content);

                    if (response.StatusCode != HttpStatusCode.OK &&
                        response.StatusCode != HttpStatusCode.NotFound)
                        throw new Exception("Error");

                    //if (response.Data != null)
                    //{ }
                    //response.Data.ForEach(result.Add);
                    //Newtonsoft.Json jsonConverter = new Newtonsoft.Json();

                    return result.Nome;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erro : " + ex);
                    Console.WriteLine("Mensagem : " + ex.Message);
                    return "Erro: " + ex.Message;
                }
            }

            // HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://contactcenter1.aec.com.br/WebServices/AeC.Web.Services.Hominum/api/Hierarquia?$filter=Funcionario/Matricula eq '" + matricula + "' and Substituida eq false and Status eq '1'&$select=Funcionario/Nome");
            // req.Method = "GET";
            // req.Headers.Add("token-ws", "921406d1-88aa-46cd-b793-1f36e5d6fda9");
            // req.AllowAutoRedirect = true;

            // ServicePointManager.ServerCertificateValidationCallback = delegate (
            // Object obj, X509Certificate certificate, X509Chain chain,
            // SslPolicyErrors errors)
            // {
            //     return (true);
            // };

            // //allows for validation of SSL conversations



            //ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

            // ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
            //     | SecurityProtocolType.Tls11
            //     | SecurityProtocolType.Tls12
            //     | SecurityProtocolType.Ssl3;

            // WebResponse respon = req.GetResponse();

            // Stream res = respon.GetResponseStream();

            // string ret = "";
            // byte[] buffer = new byte[1048];
            // int read = 0;
            // while ((read = res.Read(buffer, 0, buffer.Length)) > 0)
            // {
            //     //Console.Write(Encoding.ASCII.GetString(buffer, 0, read));
            //     ret += Encoding.ASCII.GetString(buffer, 0, read);
            // }
            // return ret;

        }
    }

    public class Rootobject
    {
        public Class1[] Property1 { get; set; }
    }

    public class Class1
    {
        public Funcionario Funcionario { get; set; }
    }

    public class Funcionario
    {
        public string Nome { get; set; }
    }
}
